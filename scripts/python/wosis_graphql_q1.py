import requests
import json
import pandas as pd

# GraphQL query
query = """
query MyQuery {
  wosisLatestProfiles(first: 5) {
    continent
    region
    countryName
    datasetCode
    latitude
    longitude
    geomAccuracy
    profileCode
  }
}
"""
# GraphQL endpoint
url='https://graphql.isric.org/wosis/graphql'
# Send POST request
r = requests.post(url, json={'query': query})
# Print status_code
print(r.status_code)
# Parse JSON
parsed = json.loads(r.text)
# Convert to pandas dataframe
df = pd.json_normalize(parsed['data']['wosisLatestProfiles']) 
# print dataframe
print(df)
